package com.cse.news.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cse.news.R;
import com.cse.news.activities.HomeActivityq;
import com.cse.news.adapters.HomeRecyclerViewAdapterq;
import com.cse.news.domain.api.INewsRequestApi;
import com.cse.news.domain.api.NewsRequestApi;
import com.cse.news.domain.models.articles.Article;
import com.cse.news.domain.models.articles.ArticleResponse;
import com.cse.news.domain.models.articles.Channel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeViewPagerFragmentq.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeViewPagerFragmentq#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeViewPagerFragmentq extends Fragment {

    boolean attemptToRefresh = false;

    String channelName;
    List<Article> articleList = null;
    RecyclerView recyclerView;
    ProgressBar progressBar;

    private OnFragmentInteractionListener mListener;
    private HomeActivityq homeActivityq;

    public HomeViewPagerFragmentq() {
        // Required empty public constructor
    }

    public static HomeViewPagerFragmentq newInstance(String string) {
        HomeViewPagerFragmentq fragment = new HomeViewPagerFragmentq();
        Bundle args = new Bundle();
        args.putString("ChannelName", string);
        fragment.setArguments(args);
        return fragment;
    }

    //TODO ===> Invalid
   /* @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (articleList == null) {
            if (getArguments() != null) {
                channelName = getArguments().getString("ChannelName");
                getFragmentData();
            }
        }
    }*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home_view_pager_fragmentq, container, false);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext(), LinearLayoutManager.VERTICAL, false);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);

        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(linearLayoutManager);

        if (articleList != null) {
            HomeRecyclerViewAdapterq homeRecyclerViewAdapterq =
                    new HomeRecyclerViewAdapterq(HomeViewPagerFragmentq.this.getContext(), articleList);
            recyclerView.setAdapter(homeRecyclerViewAdapterq);
        }

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (articleList == null) {
            if (getArguments() != null) {
                channelName = getArguments().getString("ChannelName");
                getFragmentData();
            }
        }
    }

    public void getFragmentData() {
        INewsRequestApi iNewsRequestApi = NewsRequestApi.getApiService();
        Call<ArticleResponse> articleResponseCall = null;

        switch (channelName) {
            case "Trang chủ":
                articleResponseCall = iNewsRequestApi.getTrangChu();
                break;
            case "Thời sự":
                articleResponseCall = iNewsRequestApi.getThoiSu();
                break;
            case "Thế giới":
                articleResponseCall = iNewsRequestApi.getTheGioi();
                break;
            case "Số hoá":
                articleResponseCall = iNewsRequestApi.getSoHoa();
                break;
            case "Pháp luật":
                articleResponseCall = iNewsRequestApi.getPhapLuat();
                break;
            case "Tâm sự":
                articleResponseCall = iNewsRequestApi.getTamSu();
                break;
            case "Cười":
                articleResponseCall = iNewsRequestApi.getCuoi();
                break;
            case "Thể thao":
                articleResponseCall = iNewsRequestApi.getTheThao();
                break;
            case "Sức khoẻ":
                articleResponseCall = iNewsRequestApi.getSucKhoe();
                break;
        }

        progressBar.setVisibility(View.VISIBLE);

        articleResponseCall.enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                progressBar.setVisibility(View.GONE);
                if(response.body() != null){
                    Channel channel = response.body().channel;

                    if (homeActivityq != null) {
                        articleList = channel.articles;
                        HomeRecyclerViewAdapterq homeRecyclerViewAdapterq =
                                new HomeRecyclerViewAdapterq(homeActivityq, articleList);
                        recyclerView.setAdapter(homeRecyclerViewAdapterq);
                    }
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                // LOI XAY RA
                // => CO THE DO LOI KET NOI MANG

                if (homeActivityq != null) {
                    Toast.makeText(homeActivityq, "Connection unavailable", Toast.LENGTH_LONG);
                    
                }


            }
        });

    }

//    public boolean canScrollUp(View view) {
//        if (android.os.Build.VERSION.SDK_INT < 14) {
//            if (view instanceof AbsListView) {
//                final AbsListView absListView = (AbsListView) view;
//                return absListView.getChildCount() > 0
//                        && (absListView.getFirstVisiblePosition() > 0 || absListView
//                        .getChildAt(0).getTop() < absListView.getPaddingTop());
//            } else {
//                return view.getScrollY() > 0;
//            }
//        } else {
//            return ViewCompat.canScrollVertically(view, -1);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        homeActivityq = (HomeActivityq) context;
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;

        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        homeActivityq = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void moveRecyclerViewToDistance(float dy);
    }
}
