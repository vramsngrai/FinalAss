package com.cse.news.utils;

import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import com.cse.news.R;
import com.cse.news.activities.HomeActivityq;

/**
 * Created by tmquoc on 09/05/2017.
 */

public class RefreshAsyncTaskq  extends AsyncTask<Void, Void, Void>{

    HomeActivityq homeActivityq;

    public RefreshAsyncTaskq(HomeActivityq homeActivityq) {
        this.homeActivityq = homeActivityq;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        homeActivityq.isRefreshing = true;
//        homeActivityq.findViewById(R.id.constraintLayoutRefreshProgress).setVisibility(View.VISIBLE);
    }

    @Override
    protected Void doInBackground(Void... params) {
        final int SLEEP_MILISEC = 3000;

        try {
            Thread.sleep(SLEEP_MILISEC);
        }
        catch (InterruptedException e) {
            Log.e("m00n", e.getMessage(),  e);
        }

        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        homeActivityq.isRefreshing = false;
//        homeActivityq.findViewById(R.id.constraintLayoutRefreshProgress).setVisibility(View.GONE);
    }
}
