package com.cse.news.adapters;



import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.cse.news.fragments.HomeViewPagerFragmentq;

/**
 * Created by tmquoc on 07/05/2017.
 */

public class HomeViewPagerAdapterq extends FragmentStatePagerAdapter {

    final String[] channelNameList = new String[] {
            "Trang chủ",
            "Thời sự",
            "Thế giới",
            "Số hoá",
            "Pháp luật",
            "Tâm sự",
            "Cười",
//            "Thể thao",
            "Sức khoẻ"
    };


    public HomeViewPagerAdapterq (FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return channelNameList[position];
    }

    @Override
    public Fragment getItem(int position) {

        return HomeViewPagerFragmentq.newInstance(channelNameList[position]);
    }

    @Override
    public int getCount() {
        return channelNameList.length;
    }


}
