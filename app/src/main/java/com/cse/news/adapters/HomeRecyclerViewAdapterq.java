package com.cse.news.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.cse.news.R;
import com.cse.news.activities.WebViewArticle;
import com.cse.news.domain.models.articles.Article;
import com.cse.news.views.MyImageView;

import java.util.List;

/**
 * Created by tmquoc on 07/05/2017.
 */

public class HomeRecyclerViewAdapterq extends RecyclerView.Adapter<HomeRecyclerViewAdapterq.ViewHolder> {

    LayoutInflater layoutInflater;
    List<Article> articleList;

    Context context;

    public HomeRecyclerViewAdapterq(Context context, List<Article> articleList) {
        super();
        this.context = context;
        this.articleList = articleList;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.home_itemq, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return  viewHolder;
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Article article = articleList.get(position);

        holder.textViewTitle.setText(article.getTitle());
        holder.textViewSource.setText("vnexpress.net");
        holder.textViewDate.setText(article.getDate());
        holder.imageView.loadImage(article.getThumbUrl());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startReadingNewsActivity();
            }

            private void startReadingNewsActivity() {
                Intent intent = new Intent(context, WebViewArticle.class);
                intent.putExtra(WebViewArticle.EXTRA_NEWS, article);
                context.startActivity(intent);
            }
        });

    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle;
        TextView textViewSource;
        TextView textViewDate;
        MyImageView imageView;

        ViewHolder(View view) {
            super(view);

            textViewTitle = (TextView) view.findViewById(R.id.textViewTitle);
            textViewSource = (TextView) view.findViewById(R.id.textViewSource);
            textViewDate = (TextView) view.findViewById(R.id.textViewDate);
            imageView = (MyImageView) view.findViewById(R.id.imageView);
        }
    }
}
