package com.cse.news.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.cse.news.R;
import com.cse.news.domain.api.NewsRequestApi;
import com.cse.news.domain.models.articles.Article;
import com.cse.news.domain.models.articles.ArticleResponse;
import com.cse.news.domain.models.articles.Channel;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*NewsRequestApi.getTestStringApiService().getTestTrangChuString().enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Toast.makeText(getApplicationContext(), "TEST -> " + response.body(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "ERROR", Toast.LENGTH_SHORT ).show();
            }
        });*/

        test();
    }
/*
@des: TEST BACKEND - LOAD NEWS
* */
    private void test() {
        NewsRequestApi.getApiService().getTrangChu().enqueue(new Callback<ArticleResponse>() {
            @Override
            public void onResponse(Call<ArticleResponse> call, Response<ArticleResponse> response) {
                if(response.body() != null){
                    Channel channel = response.body().channel;
                    //LIST ARTICLE
                    List<Article> articles = channel.articles;
                    if(articles.size() > 0){ // CHECK SU CHAC CHAN RANG BAO DA DUOC LOAD THANH CONG
                        Article article = articles.get(0);
                        Log.d("MainActivity.test()", article.getTitle() + " --- DES:" + article.description+ "LINK:" + article.getLink());
                    }
                }
            }

            @Override
            public void onFailure(Call<ArticleResponse> call, Throwable t) {
                // LOI XAY RA
                // => CO THE DO LOI KET NOI MANG
            }
        });
    }
}
