package com.cse.news.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cse.news.R;
import com.cse.news.domain.models.articles.Article;

public class WebViewArticle extends AppCompatActivity {

    public static final String EXTRA_NEWS = "EXTRA_NEWS";

    WebView webView;
    ProgressBar progressBar;
    TextView tvTitle;
    View commentLayout;

    Article article;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_view_article);
        Intent it = getIntent();
        if (!it.hasExtra(EXTRA_NEWS)) {
            Toast.makeText(getApplicationContext(), "Lỗi, không hợp lệ", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }
        article = (Article) it.getSerializableExtra(EXTRA_NEWS);
        addToolBar();
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        commentLayout = findViewById(R.id.layout_bottom);
        commentLayout.setVisibility(View.GONE);
        setupWebView();
    }

    private void setupWebView() {
        progressBar.setVisibility(View.VISIBLE);
        webView = (WebView) findViewById(R.id.webView);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(article.getLink());
        webView.setWebChromeClient(new WebChromeClient() {
                                       @Override
                                       public void onProgressChanged(WebView view, int newProgress) {
                                           super.onProgressChanged(view, newProgress);
                                           progressBar.setProgress(newProgress);
                                           if (newProgress >= 100) {
                                               progressBar.setVisibility(View.GONE);
                                               commentLayout.setVisibility(View.VISIBLE);
                                           } else {
                                               progressBar.setVisibility(View.VISIBLE);

                                           }
                                       }
                                   }

        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_browser, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId() == R.id.mnShare){
            Intent shareIntent = new Intent();
            shareIntent.setAction(Intent.ACTION_SEND);
            shareIntent.putExtra(Intent.EXTRA_TEXT, article.getLink());
            shareIntent.setType("text/plain");
            startActivity(Intent.createChooser(shareIntent, "Chia sẻ"));
        }

        return super.onOptionsItemSelected(item);
    }

    private void addToolBar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                WebViewArticle.this.finish();
            }
        });
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvTitle.setText(article.getTitle());
    }


}
