package com.cse.news.views;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.text.TextUtils;
import android.util.AttributeSet;

import com.bumptech.glide.Glide;


public class MyImageView extends AppCompatImageView {
    public MyImageView(Context context) {
        this(context, null);
    }

    public MyImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void loadImage(String path) {
        Glide.with(getContext()).load(path).centerCrop().dontAnimate().into(this);
    }

    public void loadImage(String path, int placeholder) {
        String url = "//";
        if (!TextUtils.isEmpty(path)) {
            url = path;
        }
        Glide.with(getContext()).load(url).placeholder(placeholder).centerCrop().dontAnimate().into(this);
    }
}
