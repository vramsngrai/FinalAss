package com.cse.news.domain.models.articles;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.ArrayList;
import java.util.List;

@Root(name = "channel", strict = false)
public class Channel {

    @ElementList(inline = true, required = false, name = "item")
    public List<Article> articles = new ArrayList<>();

   /* //WTF ELEMENTS
    @Element(name = "title")
    private String title;
    @Element(name = "description", required = false)
    private String description;
    @Element(name = "image", required = false)
    private Image image;
    @Element(name = "generator", required = false)
    private String generator;
    @Element(name = "link", required = false)
    private String link;
    @Element(name = "pubDate", required = false)
    private String pubDate;*/
}