package com.cse.news.domain.api;

import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

/**
 * Created by vrams_k6per9e on 9/8/2016.
 */
public class NewsRequestApi {

    private static final String API_BASE_URL = "http://vnexpress.net/rss/";
    private static Retrofit retrofit;
    private static INewsRequestApi apiService;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(SimpleXmlConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static INewsRequestApi getTestStringApiService() {
        Retrofit retrofit = null;
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(new ToStringConverterFactory())
                    .build();
        }

        return retrofit.create(INewsRequestApi.class);
    }

    /**
     * @return
     */
    public static INewsRequestApi getApiService() {
        if (apiService == null) {
            apiService = getClient().create(INewsRequestApi.class);
        }
        return apiService;
    }

   /* public void loadNewsItems(String url, CallBack<List<NewsItemObj>> l) {
        if (l != null) {
            l.onCallBack(testLoadNewsItems());
        }

    }

    public List<NewsItemObj> testLoadNewsItems() {
        List<NewsItemObj> models = new ArrayList<>();
        NewsItemObj itemModel;
        for (int i = 0; i < 50; i++) {
            itemModel = new NewsItemObj();
            itemModel.setTime("30 minutes ago");
            itemModel.setTitle("Ferrari tuột cơ hội đánh bại Hamilton: Mặt trái của sự cẩn trọng");
            itemModel.setImgUrl("http://img.f4.thethao.vnecdn.net/2016/09/07/fer-9557-1473216863_180x108.jpg");
            models.add(itemModel);
        }
        return models;
    }*/

}
