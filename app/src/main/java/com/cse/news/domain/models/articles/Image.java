package com.cse.news.domain.models.articles;

import org.simpleframework.xml.Element;

/**
 * Created by vrams_k6per9e on 9/9/2016.
 */
@Element(name = "image", required = false)
public class Image {
    @Element(name = "url", required = false)
    public String url;
    @Element(name = "title", required = false)
    public String title;
    @Element(name = "link", required = false)
    public String link;
}