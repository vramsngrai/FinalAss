package com.cse.news.domain.api;


import com.cse.news.domain.models.articles.ArticleResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

/**
 * Created by vrams_k6per9e on 9/8/2016.
 */
public interface INewsRequestApi {

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("tin-moi-nhat.rss")
    Call<ArticleResponse> getTrangChu();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("tin-moi-nhat.rss")
    Call<String> getTestTrangChuString();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("thoi-su.rss")
    Call<ArticleResponse> getThoiSu();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("the-gioi.rss")
    Call<ArticleResponse> getTheGioi();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("so-hoa.rss")
    Call<ArticleResponse> getSoHoa();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("phap-luat.rss")
    Call<ArticleResponse> getPhapLuat();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("tam-su.rss")
    Call<ArticleResponse> getTamSu();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("cuoi.rss")
    Call<ArticleResponse> getCuoi();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("the-thao.rss")
    Call<ArticleResponse> getTheThao();

    @Headers("User-Agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/52.0.2743.116 Safari/537.36")
    @GET("suc-khoe.rss")
    Call<ArticleResponse> getSucKhoe();

/*
    GIAI_TRI("http://vnexpress.net/rss/giai-tri.rss", "Giải trí"),
    TAM_SU("http://vnexpress.net/rss/tam-su.rss", "Tâm sự"),
    KINH_DOANH("http://vnexpress.net/rss/kinh-doanh.rss", "Kinh doanh"),
    THE_GIOI("http://vnexpress.net/rss/the-gioi.rss", "Thế giới"),
    THE_THAO("http://vnexpress.net/rss/the-thao.rss", "Thể thao");*/
}
