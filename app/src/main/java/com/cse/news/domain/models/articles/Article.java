package com.cse.news.domain.models.articles;

import android.util.Log;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Root;

import java.io.Serializable;

@Root(name = "item", strict = false)
public class Article implements Serializable {

    @Element(name = "title", required = true, data = false)
    public String title;
    @Element(name = "description", required = false)
    public String description;
    @Element(name = "pubDate", required = false)
    public String date;
    @Element(name = "link", required = false)
    public String link;

    public String thumbUrl;

    public String getThumbUrl() {
        if (thumbUrl == null) {

            int lastIndex = description.indexOf(".jpg");
            if (lastIndex == -1) {
                lastIndex = description.indexOf(".png");
            }
            if (lastIndex != -1) {
                thumbUrl = description.substring(
                        description.indexOf("src=") + 5, lastIndex + 4);
            }else{
                Log.d(" getThumbUrl()", "Not found Image url" );
            }
        }
        return thumbUrl;
    }
    /*@Element(name = "guid", required = false)
    public String guid;*/


    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public String getLink() {
        return link;
    }
}