package com.cse.news.domain.models;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.io.Serializable;
import java.util.List;

/**
 * Created by vrams_k6per9e on 9/8/2016.
 */

@Root(name = "rss", strict = false)
public class NewsModel implements Serializable {


    @ElementList(inline = false, name = "item")
    List<NewsItem> newsItems;

    public List<NewsItem> getNewsItems() {
        return newsItems;
    }

    @Root(name = "item", strict = false)
    static public class NewsItem implements Serializable {

        @Element(name = "title", required = false, data = false)
        private String title;
        @Element(name = "description", required = false)
        private String description;
        @Element(name = "pubDate", required = false)
        private String date;
        @Element(name = "link", required = false)
        private String link;

        public String getTitle() {
            return title;
        }

        public String getDescription() {
            return description;
        }

        public String getDate() {
            return date;
        }

        public String getLink() {
            return link;
        }
    }
}
